package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
//will require all routes within the DISCUSSION APPLICATION to use "/greeting" as part of its routes
@RequestMapping("/greeting")
//The "@RestController" annotation tells Spring Boot that this application will function as an endpoint that will be used in handling web requests
public class DiscussionApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}

	@GetMapping("/hello")
	public String hello(){
			return "Hello World";}

	@GetMapping("/hi")
	public String hi(@RequestParam(value = "name", defaultValue = "John") String name){
		return String.format("hi %s", name);
	}
	// Multiple PARAMETER
	@GetMapping("/friend")
	public String friend(@RequestParam(value = "name", defaultValue = "Joe") String name, @RequestParam(value="friend",defaultValue = "Jane")String friend){
		return String.format("Hello %s! My name is %s", friend, name);
	}

	//routes with path variables
	@GetMapping("hello/{name}")
	//localhost:8080/hello/joe
	//@pathvariable annotation allows us to extract Data directly from the URL.
	public String courses (@PathVariable("name")String name){
		return String.format("Nice to meet you %s!", name);
	}

	// S09 - ACITVITY
	ArrayList<String> enrollees = new ArrayList<String>();

	@GetMapping("/enroll")
	public String enrollUser(@RequestParam(value="user")String user){
		enrollees.add(user);
		return "Thank you for enrolling, "+user+"!";
	}

	@GetMapping("/getEnrollees")
	public String getEnrollees(){
		return enrollees.toString();
	}

	@GetMapping("/nameage")
	public String nameAge(@RequestParam(value="name")String name,@RequestParam(value="age") int age){
		return "Hello "+name+" My age is "+age;
	}

	@GetMapping("/courses/{id}")
	public String getCourse(@PathVariable String id){
		String courseName,schedule,price;
		if(id.equals("java101")){
			courseName = "java101";
			schedule = "MWF 8:00 AM - 11:00 AM";
			price = "PHP 3000";
		}
		else if(id.equals("sql101")){
			courseName = "sql101";
			schedule = "TTH 9:00 AM - 11:00 AM";
			price = "PHP 2500";
		}
		else if(id.equals("javaee101")){
			courseName = "javaee101";
			schedule = "MWF 1:30 PM - 4:00 PM";
			price = "PHP 4000";
		}

		else{
			return "That course cannot be found";
		}
		return "Name: "+ courseName+", "+"Schedule: "+schedule+", "+"Price: "+price;
	}



}
